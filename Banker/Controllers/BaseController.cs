﻿using Banker.Models;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using System.Web;
using System.Web.Mvc;
using System.Threading.Tasks;
using System.Web.Mvc.Filters;
using Microsoft.AspNet.Identity;
using Z.EntityFramework.Plus;

namespace Banker.Controllers
{
    public class BaseController : Controller
    {
        private ApplicationSignInManager _signInManager;
        private ApplicationUserManager _userManager;
        private ApplicationRoleManager _roleManager;
        private ApplicationDbContext _dbManager;
        private ApplicationUser _currentUser;

      
       
        public ApplicationDbContext DbManager
        {
            get
            {
                return _dbManager ?? HttpContext.GetOwinContext().Get<ApplicationDbContext>();
            }
            private set
            {
                _dbManager = value;
            }
        }

        protected override void OnAuthentication(AuthenticationContext filterContext)
        {
            ViewBag.CurrentUser = CurrentUser;
            base.OnAuthentication(filterContext);
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }


        public ApplicationRoleManager RoleManager
        {
            get
            {
                return _roleManager ?? HttpContext.GetOwinContext().Get<ApplicationRoleManager>();
            }
            private set
            {
                _roleManager = value;
            }
        }

        public ApplicationSignInManager SignInManager
        {
            get
            {
                return _signInManager ?? HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
            }
            private set
            {
                _signInManager = value;
            }
        }


        public ApplicationUser CurrentUser
        {
            get
            {
                return _currentUser ?? DbManager.Users.FirstOrDefault(u => u.UserName==User.Identity.Name);
            }
            private set
            {
                _currentUser = value;
            }
        }

        public async Task<List<SelectListItem>> GetRolesSelectListAsync()
        {
            List<SelectListItem> list = new List<SelectListItem>();
            await DbManager.Roles.ForEachAsync(r =>
                {
                   list.Add(new SelectListItem() { Text = r.Name, Value = r.Name });
                }
            );
            return list;
        }

        public async Task<List<ApplicationUser>> GetAccountOfficersAsyncFor(int bankId)
        {
            var ao = new List<ApplicationUser>();
            var users = await DbManager.Users.Where(u => u.Bank.Id == bankId).ToListAsync();
            foreach (var u in users)
            {
                if (await UserManager.IsInRoleAsync(u.Id, Role.BANK_OFFICER))
                    ao.Add(u);
            }
            return ao;
        }



        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (_userManager != null)
                {
                    _userManager.Dispose();
                    _userManager = null;
                }

                if (_signInManager != null)
                {
                    _signInManager.Dispose();
                    _signInManager = null;
                }

                if (_roleManager != null)
                {
                    _roleManager.Dispose();
                    _roleManager = null;
                }
                if (_dbManager != null)
                {
                    _dbManager.Dispose();
                    _dbManager = null;
                }
            }

            base.Dispose(disposing);
        }
       
        public BaseController()
        {
            _dbManager = new ApplicationDbContext();
            
        }

    }
}