﻿using Banker.Helpers;
using Banker.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Banker.Controllers
{
    public class HomeController : BaseController
    {
        public ActionResult Index()
        {
            if(User.Identity.IsAuthenticated)
            {
                if (User.IsInRole(Role.DGI_ADMIN))
                    return RedirectToAction("Index", "Dashboard", new { area = "DGI" });
                if (User.IsInRole(Role.BANK_OFFICER))
                    return RedirectToAction("Index", "Confirmations", new { area = "BankOfficer" });
                if (User.IsInRole(Role.BANK_AUDITOR))
                    return RedirectToAction("Index", "Reporting", new { area = "BankAuditor" });
            }

            return RedirectToAction("Login","Account");
        }

        public async Task<ActionResult> Seed()
        {
            try
            {
                await DbInitializer.SeedDataAsync(DbManager, UserManager);
                return Content("Database seed successfully");
            }
            catch (Exception ex)
            {
                throw;
                //return Content($"Database seed failed {ex.Message}");
            }
            
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";
            return View();
        }
    }
}