﻿using Banker.Models;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace Banker.Helpers
{
    public class DbInitializer
    {
        static string[] roleNames = { Role.BANK_OFFICER, Role.DGI_ADMIN, Role.MAIN_BANK_VALIDATOR, Role.BANK_AUDITOR, Role.SUPER_ADMIN };
        static string usernameDGI = "admin@admin.com";
        static string passwordDGI = "P@ssword123";
        public static async Task SeedDataAsync(ApplicationDbContext context,ApplicationUserManager userManager)
        {
            
            //Create roles
            foreach (string roleName in roleNames)
            {         
                var role = new IdentityRole(roleName);
                context.Roles.AddOrUpdate(r => r.Name, role);
            }

            //Create DGI admin user
           
            var DGIUser = new ApplicationUser() { UserName = usernameDGI,Email=usernameDGI ,Profil=Role.DGI_ADMIN,IsValidated=true };
            var result=await userManager.CreateAsync(DGIUser, passwordDGI);
            await userManager.AddToRoleAsync(DGIUser.Id, Role.DGI_ADMIN);
            //context.SaveChanges();
        }
    }
}