﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Web;

namespace Banker.Helpers
{
    public class EmailSender
    {
        private static string sender = "TaxCollect@paarib.bj";
        private static EmailSender _emailSender ;

        public static EmailSender Instance 
        {
            get { return _emailSender??new EmailSender(); ; }
            private set { _emailSender = value; }
        }

        public void Send(string to,string subject,string body,bool isHtml=false, string cc = "")
        {
            MailMessage msg = new MailMessage(sender, to, subject, body);
            msg.IsBodyHtml = isHtml;
            msg.CC.Add(cc);
            using(SmtpClient smtpclient = new SmtpClient())
            {
                smtpclient.Timeout = 300000;
                smtpclient.Send(msg);
            }           
        }

        public void SendWithAttachement(string to,string subject, string body,string attachementPath,bool isHtml=false)
        {
            MailMessage msg = new MailMessage(sender, to, subject, body);
            msg.IsBodyHtml = isHtml;
            Attachment attachment = new Attachment(attachementPath);
            msg.Attachments.Add(attachment);
            //msg.CC.Add(cc);
            using (SmtpClient smtpclient = new SmtpClient())
            {
                smtpclient.UseDefaultCredentials = true;
                smtpclient.Timeout = 300000;
                smtpclient.Send(msg);
            }
        }
        
    }
}