﻿using Banker.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Banker.ViewModels
{
    public class UsersVM
    {
    }

    public class AddBankUserVM
    {
        public string BankId { get; set; }
        public string Nom { get; set; }
        public string Prenoms { get; set; }
        public string Email { get; set; }
        public string Telephone { get; set; }
        public string Profil { get; set; }

        public string[] GetRoles()
        {
            var roles = new[] { Role.BANK_OFFICER, Role.BANK_AUDITOR};
            return roles;
        }
    }
}