﻿using Banker.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Banker.ViewModels
{
    public class BankVM
    {
        [Display(Name="Nom de la banque")]
        [Required]
        public string Name { get; set; }        
        public string Logo { get; set; }
        [Display(Name = "Numéro de téléphone")]
        [Required]
        public string PhoneNumber { get; set; }
        [Display(Name = "Email")]
        [Required]
        public string Email { get; set; }
        [Display(Name = "Adresse")]
        public string Adresse { get; set; }
        public DateTime CreatedDate { get; set; }
        public HttpPostedFileBase UploadedFile { get; set; }
    }

    public class BankDetailsVM
    {
        public BankDetailsVM()
        {
            Users = new List<ApplicationUser>();
        }
        public Bank Bank { get; set; }
        public List<ApplicationUser> Users { get; set; }
        public int Pending { get; set; }
        public int Approved { get; set; }
        public int Rejected { get; set; }
        public long CollectedAmount { get; set; }
    }
}