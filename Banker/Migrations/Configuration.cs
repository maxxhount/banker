namespace Banker.Migrations
{
    using Microsoft.AspNet.Identity;
    using Microsoft.AspNet.Identity.EntityFramework;
    using Microsoft.AspNet.Identity.Owin;
    using Models;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using System.Web;

    internal sealed class Configuration : DbMigrationsConfiguration<Banker.Models.ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(Banker.Models.ApplicationDbContext context)
        {            
            //var roleManager = HttpContext.Current.GetOwinContext().Get<ApplicationRoleManager>();
            string[] roleNames = { Role.BANK_OFFICER, Role.DGI_ADMIN,Role.MAIN_BANK_VALIDATOR,Role.BANK_AUDITOR, Role.SUPER_ADMIN };
            
            //Create roles
            foreach (string roleName in roleNames)
            {
                var role = new IdentityRole(roleName);
                context.Roles.AddOrUpdate(r => r.Name, role);
            }
            context.SaveChanges();


            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //
        }
    }
}
