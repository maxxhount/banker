﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Banker.Models;
using Banker.Controllers;
using Banker.ViewModels;
using System.IO;
using Syroot.Windows.IO;
using Microsoft.AspNet.Identity;

namespace Banker.Areas.Admin.Controllers
{
    public class BanksController : BaseController
    {

        // GET: Admin/Banks
        public async Task<ActionResult> Index()
        {
            return View(await DbManager.Banks.ToListAsync());
        }

        // GET: Admin/Banks/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Bank bank = await DbManager.Banks.FindAsync(id);
            if (bank == null)
            {
                return HttpNotFound();
            }
            var bankUsers = await DbManager.Users.Where(u => u.Bank.Id == bank.Id).ToListAsync();
            var vm = new BankDetailsVM() { Bank = bank, Users = bankUsers };
            
            return View(vm);
        }

        // GET: Admin/Banks/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Admin/Banks/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(BankVM vm)
        {
            string folder = "/Uploads";
            var RootFolder = Server.MapPath("~"+folder);
            string path = "";          
            if (ModelState.IsValid)
            {
                if(vm.UploadedFile!=null)
                {
                    var fileName = "logo_"+vm.Name + Path.GetExtension(vm.UploadedFile.FileName);
                    path = Path.Combine(RootFolder, fileName);
                    vm.UploadedFile.SaveAs(path);
                    vm.Logo = folder+"/"+fileName;
                }
                var bank = CreateBankFrom(vm);
                DbManager.Banks.Add(bank);
                await DbManager.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(vm);
        }

        [HttpPost]
        public async Task<ActionResult> AddBankAdmin(AddBankUserVM vm)
        {
            try
            {
                string bankId = vm.BankId;
                var currentBank = await DbManager.Banks.FirstOrDefaultAsync(b => b.Id.ToString() == bankId);
                var user = CreateUserFrom(vm);
                var result = await UserManager.CreateAsync(user, Common.TEMP_PASSWORD);
                if (result == IdentityResult.Success)
                {
                    await UserManager.AddToRoleAsync(user.Id, Role.DGI_ADMIN);
                    var newUser = await DbManager.Users.FirstOrDefaultAsync(u=>u.UserName==user.Email);
                    newUser.Bank = currentBank;
                    await DbManager.SaveChangesAsync();
                }


                return RedirectToAction("Details", new { id = bankId });
            }
            catch (Exception)
            {

                throw;
            }
        }

        public async Task<ActionResult> DeleteBankAdmin(string userId,string bankId)
        {
            try
            {              
                var admin = await DbManager.Users.FirstOrDefaultAsync(u => u.Id == userId);
                DbManager.Users.Remove(admin);
                await DbManager.SaveChangesAsync();
                return RedirectToAction("Details", new { id = bankId });
            }
            catch (Exception)
            {
                throw;
            }
        }

        private ApplicationUser CreateUserFrom(AddBankUserVM vm)
        {
            var user = new ApplicationUser()
            {
                CreationDate=DateTime.Now,
                Email=vm.Email,
                IsValidated=true,
                IP=Request.UserHostAddress,
                Nom=vm.Nom,
                Prenoms=vm.Prenoms,
                PhoneNumber=vm.Telephone,
                TempPassword=Common.TEMP_PASSWORD,               
                UserName=vm.Email               
            };
            return user;
        }

        private Bank CreateBankFrom(BankVM vm)
        {
            var bank = new Bank()
            {
                PhoneNumber=vm.PhoneNumber,
                Email=vm.Email,
                Adresse=vm.Adresse,
                Name = vm.Name,
                Logo = vm.Logo,
                CreatedDate = DateTime.Now
            };
            return bank;
        }

        // GET: Admin/Banks/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Bank bank = await DbManager.Banks.FindAsync(id);
            if (bank == null)
            {
                return HttpNotFound();
            }
            return View(bank);
        }

        // POST: Admin/Banks/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Id,Name,Logo")] Bank bank)
        {
            if (ModelState.IsValid)
            {
                DbManager.Entry(bank).State = EntityState.Modified;
                await DbManager.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(bank);
        }

        // GET: Admin/Banks/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            try
            {              
                Bank bank = await DbManager.Banks.FindAsync(id);
                if (bank != null)
                {
                    DbManager.Banks.Remove(bank);
                    await DbManager.SaveChangesAsync();
                    return RedirectToAction("Index");
                }
                return HttpNotFound();
            }
            catch
            {
                return View("Error");
            }
            
            
        }

        // POST: Admin/Banks/Delete/5
        
    }
}
