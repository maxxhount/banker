﻿using Banker.Areas.DGI.ViewModels;
using Banker.Controllers;
using Banker.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Banker.Areas.DGI.Controllers
{
    [Authorize(Roles = "DGI_ADMIN")]
    public class DashboardController : BaseController
    {
        // GET: Admin/Dashboard
        public async Task<ActionResult> Index()
        {
            var vm = new DashboardVM
            {
                CollectedAmount=GetTotal(),
                Approved = GetApproved(),
                Pending = GetPending(),
                Rejected = GetRejected(),
                Declarations= await GetRecentsDeclarations()
            };
            return View(vm);
        }

        public int GetPending()
        {
            var pending = DbManager.Declarations.Where(d => d.Status == DeclarationStatus.PENDING).ToList().Count();
            return pending;
        }

        public int GetApproved()
        {
            var pending = DbManager.Declarations.Where(d => d.Status == DeclarationStatus.APPROVED).ToList().Count();
            return pending;
        }

        public int GetRejected()
        {
            var pending = DbManager.Declarations.Where(d => d.Status == DeclarationStatus.REJECTED).ToList().Count();
            return pending;
        }
        public long GetTotal()
        {
            var amounts = DbManager.Declarations.Where(d => d.Status == DeclarationStatus.APPROVED).ToList().Where(d=> IsTodayTransaction(d.ValidationDate.Value)).Select(d=>d.Amount);

            return amounts!=null &&amounts.Count()>0?amounts.Sum():0;
        }

        public async Task<List<Declaration>> GetRecentsDeclarations()
        {
            var decs =await DbManager.Declarations.OrderByDescending(d => d.CreatedDate).Take(10).ToListAsync();
            return decs;
        }

        private bool IsTodayTransaction(DateTime validationDate)
        {
            bool ok = validationDate.Year == DateTime.Now.Year
                      && validationDate.Month == DateTime.Now.Month
                      && validationDate.Day == DateTime.Now.Day;
            return ok;
        }
    }
}