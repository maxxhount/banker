﻿using Banker.Areas.DGI.ViewModels;
using Banker.Controllers;
using Banker.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Banker.Areas.DGI.Controllers
{
    [Authorize(Roles = "DGI_ADMIN")]
    public class ReportingController : BaseController
    {
        // GET: DGI/Reporting
        public async Task<ActionResult> Index()
        {
            var vm = new ReportingVM();
            vm.Banks = await DbManager.Banks.ToListAsync();
            vm.Companies = await DbManager.Companies.ToListAsync();
            ViewBag.IsPost = false;
            return View(vm);
        }

        [HttpPost]
        public async Task<ActionResult> Index(FormCollection form,ReportingVM vm)
        {
            try
            {
                var dateParts = form["daterange"].Split('-');
                var from = DateTime.ParseExact(dateParts[0].Trim(), "MM/dd/yyyy", CultureInfo.InvariantCulture);
                var to = DateTime.ParseExact(dateParts[1].Trim(), "MM/dd/yyyy", CultureInfo.InvariantCulture);
                var enterpriseId = form["enterprise"];
                var status = form["status"];
                var banqueId = form["bank"];
                var decl = await DbManager.Declarations.Where(d => d.CreatedDate.Value <= to && d.CreatedDate.Value >= from).ToListAsync();

                if (!string.IsNullOrEmpty(enterpriseId))
                {
                    decl = decl.Where(d => d.Company.Id == int.Parse(enterpriseId)).ToList();
                }
                if(!string.IsNullOrEmpty(banqueId))
                {
                    decl= decl.Where(d => d.Bank.Id == int.Parse(banqueId)).ToList();
                }
                if(!string.IsNullOrEmpty(status))
                {
                    if (status == DeclarationStatus.APPROVED)
                        decl = decl.Where(d => d.Status == DeclarationStatus.APPROVED).ToList();
                    else if (status == DeclarationStatus.CONFIRMED)
                        decl = decl.Where(d => d.Status == DeclarationStatus.CONFIRMED).ToList();
                    else if (status == DeclarationStatus.PENDING)
                        decl = decl.Where(d => d.Status == DeclarationStatus.PENDING).ToList();
                    else if (status == DeclarationStatus.REJECTED)
                        decl = decl.Where(d => d.Status == DeclarationStatus.REJECTED).ToList();
                }
                vm.Banks = await DbManager.Banks.ToListAsync();
                vm.Companies = await DbManager.Companies.ToListAsync();
                vm.Declarations = decl;
                ViewBag.IsPost = true;
                ViewBag.From = from;
                ViewBag.To = to;
                return View(vm);
            }
            catch
            {
                throw;
            }
            
        }
    }
}