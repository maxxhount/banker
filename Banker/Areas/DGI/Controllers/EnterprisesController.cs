﻿using Banker.Areas.BankAdmin.ViewModels;
using Banker.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Threading.Tasks;
using System.Data.Entity;
using Banker.Models;
using System.IO;
using OfficeOpenXml;
using System.Data.Entity.Migrations;

namespace Banker.Areas.DGI.Controllers
{
    [Authorize(Roles = "DGI_ADMIN")]
    public class EnterprisesController : BaseController
    {
        // GET: BankAdmin/Enterprises
        public async  Task<ActionResult> Index()
        {
            var addVM = new AddEnterpriseVM();
            ViewBag.PartialVM = addVM;
            var enterprises = DbManager.Companies.ToList();
            return View(enterprises);
        }

        [HttpPost]
        public async Task<ActionResult> Add(AddEnterpriseVM vm)
        {
            try
            {
                var currentBank = CurrentUser.Bank;
                var company = GetCompanyFrom(vm);
                DbManager.Companies.Add(company);
                await DbManager.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            catch (Exception)
            {
                throw;
            }
        }



        public async Task<ActionResult> Delete(int Id)
        {
            try
            {
                var company = await DbManager.Companies.FindAsync(Id);
                if (company != null)
                {
                    DbManager.Companies.Remove(company);
                    await DbManager.SaveChangesAsync();
                    return RedirectToAction("Index");
                }
                return HttpNotFound();
            }
            catch
            {
                throw;
            }
        }

        public async Task<ActionResult> Edit(int Id)
        {
            var company = await DbManager.Companies.FindAsync(Id);
            return View(company);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Id,Nom,IFU,CreatedDate,PhoneNumber,Email,TransferNumber")] Company company)
        {
            if (ModelState.IsValid)
            {
                DbManager.Entry(company).State = EntityState.Modified;
                await DbManager.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(company);
        }

        [HttpPost]
        public async Task<ActionResult> UploadEnterprises()
        {
            if (Request != null)
            {

                try
                {
                    var file = Request.Files["uploadedFile"];
                    if ((file != null) && (file.ContentLength > 0) && !string.IsNullOrEmpty(file.FileName))
                    {
                        int nbrAddedDeposit = await ReadExcelFile(file.InputStream);
                        TempData["uploadSuccess"] = true;
                        TempData["items"] = nbrAddedDeposit.ToString();
                    }
                }
                catch (Exception)
                {
                    TempData["uploadSuccess"] = false;
                    //throw;
                    
                }

            }



            return RedirectToAction("Index");
        }

        private async Task<int> ReadExcelFile(Stream inputStream)
        {
            using (ExcelPackage package = new ExcelPackage(inputStream))
            {
                var sheet = package.Workbook.Worksheets.FirstOrDefault();
                var nbrCol = sheet.Dimension.End.Column;
                var nbrRow = sheet.Dimension.End.Row;
                for (int i = 2; i <= nbrRow; i++)
                {
                    var c = new Company();
                    c.IFU = sheet.Cells[i, 1].Value.ToString();
                    c.Nom = sheet.Cells[i, 2].Value.ToString().Trim();
                    c.TransferNumber = sheet.Cells[i, 3].Value.ToString().Trim().Replace(" ", "");
                    var date = sheet.Cells[i, 4].Value.ToString().Trim();
                    c.CreatedDate = CreateDateFrom(date);
                    DbManager.Companies.AddOrUpdate(cx => new { cx.IFU }, c);
                    await DbManager.SaveChangesAsync();
                }
                return nbrRow - 1;
            }
        }

        private DateTime CreateDateFrom(string date)
        {
            long dateNum = long.Parse(date);
            DateTime result = DateTime.FromOADate(dateNum);

            return result;
        }

        private Company GetCompanyFrom(AddEnterpriseVM vm)
        {
            var c = new Company()
            {
                TransferNumber = vm.TransferNumber,
                IFU = vm.IFU,
                Nom = vm.Nom,
                CreatedDate=DateTime.Now
            };
            return c;
        }

        private async Task<List<SelectListItem>> GetAccountOfficersSelectListForAsync(int bankId)
        {
            var list = new List<SelectListItem>();
            var ao = await GetAccountOfficersAsyncFor(bankId);            
            ao.ForEach(u => { list.Add(new SelectListItem { Text = u.Email, Value = u.Email }); });
            return list;
        }

        

    }
}