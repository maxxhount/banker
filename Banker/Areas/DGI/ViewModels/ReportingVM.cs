﻿using Banker.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Banker.Areas.DGI.ViewModels
{
    public class ReportingVM
    {
        public ReportingVM()
        {
            Declarations = new List<Declaration>();
            Companies = new List<Models.Company>();
            Banks = new List<Models.Bank>();
        }
        public List<Company> Companies { get; set; }
        public List<Bank> Banks { get; set; }
        public List<Declaration> Declarations { get; set; }
    }
}