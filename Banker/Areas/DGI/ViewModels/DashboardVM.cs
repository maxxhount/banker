﻿using Banker.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Banker.Areas.DGI.ViewModels
{
    public class DashboardVM
    {
        public DashboardVM()
        {
            Declarations = new List<Declaration>();
        }
        public int Pending { get; set; }
        public int Approved{ get; set; }
        public int Rejected { get; set; }
        public long CollectedAmount { get; set; }
        public List<Declaration> Declarations { get; set; }
    }

    public class BankDashboardVM
    {
        public BankDashboardVM()
        {
        }
        public int Pending { get; set; }
        public int Approved { get; set; }
        public int Rejected { get; set; }
        public long CollectedAmount { get; set; }
        public Bank Bank { get; set; }
    }


}