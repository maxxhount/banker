﻿using System.Web.Mvc;

namespace Banker.Areas.DGI
{
    public class DGIAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "DGI";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "DGI_default",
                "DGI/{controller}/{action}/{id}",
                new { action = "Index",controller="Dashboard", id = UrlParameter.Optional }
            );
        }
    }
}