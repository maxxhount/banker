﻿using Banker.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Banker.Areas.BankAdmin.Controllers
{
    [Authorize]
    public class DashboardController : BaseController
    {
        // GET: BankAdmin/Dashboard
        public ActionResult Index()
        {
            return View();
        }
    }
}