﻿using Banker.Areas.BankAdmin.ViewModels;
using Banker.Controllers;
using Banker.Models;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Banker.Areas.BankAdmin.Controllers
{
    public class UsersController : BaseController
    {

        // GET: BankAdmin/Users
        public async Task<ActionResult> Index()
        {
            var addUserVM = new AddUserVM();
            addUserVM.Roles = await GetRolesSelectListAsync();
            ViewBag.PartialVM = addUserVM;
            var users = DbManager.Users.Where(u=>u.Bank.Id==CurrentUser.Bank.Id).ToList();
            return View(users);
        }

        public async Task<ActionResult> Add(AddUserVM vm)
        {
            try
            {
                var currentBank = CurrentUser.Bank;
                var user = GetUSerFrom(vm);
                var result = await UserManager.CreateAsync(user, Common.TEMP_PASSWORD);
                if (result == IdentityResult.Success)
                {
                    await UserManager.AddToRoleAsync(user.Id, vm.Role);
                    var newUser = await DbManager.Users.FirstOrDefaultAsync(u => u.UserName == vm.Email);
                    newUser.Bank = currentBank;
                    await DbManager.SaveChangesAsync();
                }
                return RedirectToAction("Index");
            }
            catch (Exception)
            {
                throw;
            }
        }

        private ApplicationUser GetUSerFrom(AddUserVM vm)
        {
            var user = new ApplicationUser()
            {
                CreationDate = DateTime.Now,
                Email = vm.Email,
                IsValidated = true,
                IP = Request.UserHostAddress,
                Nom = vm.Nom,
                Prenoms = vm.Prenoms,
                PhoneNumber = vm.Telephone,
                TempPassword = Common.TEMP_PASSWORD,
                UserName = vm.Email,
                Profil=vm.Role
                
            };
            return user;
        }
    }
}