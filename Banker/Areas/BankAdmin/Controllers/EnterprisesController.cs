﻿using Banker.Areas.BankAdmin.ViewModels;
using Banker.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Threading.Tasks;
using System.Data.Entity;
using Banker.Models;

namespace Banker.Areas.BankAdmin.Controllers
{
    public class EnterprisesController : BaseController
    {
        // GET: BankAdmin/Enterprises
        public async  Task<ActionResult> Index()
        {
            var addVM = new AddEnterpriseVM();
            ViewBag.PartialVM = addVM;
            var enterprises = DbManager.Companies.ToList();
            return View(enterprises);
        }

        [HttpPost]
        public async Task<ActionResult> Add(AddEnterpriseVM vm)
        {
            try
            {
                var currentBank = CurrentUser.Bank;
                var company = GetCompanyFrom(vm);
                DbManager.Companies.Add(company);
                await DbManager.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            catch (Exception)
            {
                throw;
            }
        }

        private Company GetCompanyFrom(AddEnterpriseVM vm)
        {
            var c = new Company()
            {
                TransferNumber = vm.TransferNumber,
                IFU = vm.IFU,
                Nom = vm.Nom,
                CreatedDate=DateTime.Now
            };
            return c;
        }

        private async Task<List<SelectListItem>> GetAccountOfficersSelectListForAsync(int bankId)
        {
            var list = new List<SelectListItem>();
            var ao = await GetAccountOfficersAsyncFor(bankId);            
            ao.ForEach(u => { list.Add(new SelectListItem { Text = u.Email, Value = u.Email }); });
            return list;
        }

        

    }
}