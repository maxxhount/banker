﻿using System.Web.Mvc;

namespace Banker.Areas.BankAdmin
{
    public class BankAdminAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "BankAdmin";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "BankAdmin_default",
                "BankAdmin/{controller}/{action}/{id}",
                new { action = "Index",controller="Dashboard", id = UrlParameter.Optional }
            );
        }
    }
}