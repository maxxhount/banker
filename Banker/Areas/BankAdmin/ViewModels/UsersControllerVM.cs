﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Banker.Areas.BankAdmin.ViewModels
{
    public class UsersControllerVM
    {
    }

    public class AddUserVM
    {
        public string Nom { get; set; }
        public string Prenoms { get; set; }
        public string Email { get; set; }
        public string Telephone { get; set; }
        [Required(ErrorMessage = "Veuillez choisir le role")]
        public List<SelectListItem> Roles { get; set; }
        public string Role { get; set; }
    }
}