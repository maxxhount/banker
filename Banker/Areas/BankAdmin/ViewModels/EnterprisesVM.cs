﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Banker.Areas.BankAdmin.ViewModels
{
    public class EnterprisesVM
    {
    }

    public class AddEnterpriseVM
    {
        [Display(Name ="Raison sociale")]
        [Required]
        public string Nom { get; set; }
        public string IFU { get; set; }
        public string Email { get; set; }
        [Display(Name = "Numero de téléphone")]
        [Required]
        public string PhoneNumber { get; set; }
        [Display(Name = "Numero de virement")]
        [Required]
        public string TransferNumber  { get; set; }
       
    }
}