﻿using Banker.Areas.DGI.ViewModels;
using Banker.Controllers;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Banker.Areas.BankAuditor.Controllers
{
    public class ReportingController : BaseController
    {
        // GET: DGI/Reporting
        public ActionResult Index()
        {         
            ViewBag.IsPost = false;
            return View();
        }

        [HttpPost]
        public async Task<ActionResult> Index(FormCollection form,ReportingVM vm)
        {
            try
            {
                var bankId = CurrentUser.Bank.Id;
                var dateParts = form["daterange"].Split('-');
                var from = DateTime.ParseExact(dateParts[0].Trim(), "MM/dd/yyyy", CultureInfo.InvariantCulture);
                var to = DateTime.ParseExact(dateParts[1].Trim(), "MM/dd/yyyy", CultureInfo.InvariantCulture);
                var status = form["status"];
                var decl = await DbManager.Declarations.Where(d=>d.Bank.Id==bankId).Where(d => d.CreatedDate.Value <= to && d.CreatedDate.Value >= from).ToListAsync();
                vm.Declarations = decl;
                ViewBag.IsPost = true;
                return View(vm);
            }
            catch
            {
                throw;
            }
            
        }
    }
}