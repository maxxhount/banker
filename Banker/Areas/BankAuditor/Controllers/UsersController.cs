﻿using Banker.Controllers;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Banker.Areas.BankAuditor.Controllers
{
    public class UsersController : BaseController
    {
        // GET: BankAuditor/Users
        public async Task<ActionResult> Index()
        {
            var bankId= CurrentUser.Bank.Id;
            var users = await DbManager.Users.Where(u => u.Bank.Id == bankId).ToListAsync();
            return View(users);
        }
    }
}