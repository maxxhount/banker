﻿using System.Web.Mvc;

namespace Banker.Areas.BankAuditor
{
    public class BankAuditorAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "BankAuditor";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "BankAuditor_default",
                "BankAuditor/{controller}/{action}/{id}",
                new { action = "Index",controller="Users", id = UrlParameter.Optional }
            );
        }
    }
}