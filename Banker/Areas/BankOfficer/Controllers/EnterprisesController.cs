﻿using Banker.Controllers;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Banker.Areas.BankOfficer.Controllers
{
    
    public class EnterprisesController : BaseController
    {
        // GET: AccountOfficer/Entreprises
        public async Task<ActionResult> Index()
        {
            var currenUserName = "officer@ubagroup.com";
            var companies = await DbManager.Companies.ToListAsync();
            return View(companies);
        }
    }
}