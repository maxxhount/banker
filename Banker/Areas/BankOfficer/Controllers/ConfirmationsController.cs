﻿using Banker.Controllers;
using Banker.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Banker.Areas.BankOfficer.Controllers
{
    [Authorize]
    public class ConfirmationsController : BaseController
    {
        // GET: AccountOfficer/Confirmations
        public async Task<ActionResult> Index(string status="pending")
        {
            var bankId = CurrentUser.Bank.Id;
            ViewBag.Current = status;
            var decs = await DbManager.Declarations.Where(d => d.Bank.Id == bankId && d.Status==DeclarationStatus.PENDING).ToListAsync();
            switch (status)
            {
                case "approved":
                    decs= await DbManager.Declarations.Where(d => d.Bank.Id == bankId && d.Status == DeclarationStatus.APPROVED).ToListAsync();
                    break;
                case "rejected":
                    decs = await DbManager.Declarations.Where(d => d.Bank.Id == bankId && d.Status == DeclarationStatus.REJECTED).ToListAsync();
                    break;
                default:
                    break;
            }
            return View(decs);
        }

        public PartialViewResult Reject(string Id)
        {
            ViewBag.DeclarationId = Id;
            return PartialView("_PaymentRejectPartial");
        }

        [HttpPost]
        public async Task<ActionResult> Reject(FormCollection form)
        {
            string declarationId = form["DeclId"];
            string reason = form["reason"];
            if (string.IsNullOrEmpty(reason))
            {
                TempData["reasonIsNull"] = true;
            }
            else
            {
                var dec = await DbManager.Declarations.SingleOrDefaultAsync(d => d.Id == declarationId);
                dec.Status = DeclarationStatus.REJECTED;
                dec.Validator = CurrentUser;
                dec.RejectionReason = reason;
                await DbManager.SaveChangesAsync();
            }
            return Redirect("Index");
        }

        public PartialViewResult Confirm(string Id)
        {
            ViewBag.DeclarationId = Id;
            return PartialView("_PaymentConfirmPartial");
        }

        [HttpPost]
        public async Task<ActionResult> Confirm(FormCollection form)
        {
            string declarationId = form["DeclId"];
            var dec = await DbManager.Declarations.SingleOrDefaultAsync(d => d.Id == declarationId);
            dec.Status = DeclarationStatus.APPROVED;
            dec.Validator = CurrentUser;
            dec.ValidationDate = DateTime.Now;
            await DbManager.SaveChangesAsync();
            return Redirect("Index");
        }
    }
}