﻿using System.Web.Mvc;

namespace Banker.Areas.BankOfficer
{
    public class BankOfficerAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "BankOfficer";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "BankOfficer_default",
                "BankOfficer/{controller}/{action}/{id}",
                new { action = "Index",controller="Confirmations", id = UrlParameter.Optional }
            );
        }
    }
}