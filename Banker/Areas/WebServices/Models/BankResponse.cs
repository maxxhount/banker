﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Banker.Areas.WebServices.Models
{
    public class BankResponse
    {
        public string Logo { get; set; }
        public string Name { get; set; }
        public string Id { get; set; }
    }
}