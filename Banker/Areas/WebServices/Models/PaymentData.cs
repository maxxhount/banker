﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Banker.Areas.WebServices.Models
{
    public class PaymentData
    {
        public string IFU { get; set; }
        public string Amount { get; set; }
        public string CartId { get; set; }
        public string BankId { get; set; }
        public string CompanyName { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }

    }
}