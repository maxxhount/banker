﻿using Banker.Areas.WebServices.Models;
using Banker.Helpers;
using Banker.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace Banker.Areas.WebServices.Controllers
{
    public class OnlinePaymentController : ApiController
    {
        private ApplicationDbContext _dbManager;
        public ApplicationDbContext DbManager
        {
            get
            {
                return _dbManager ?? HttpContext.Current.GetOwinContext().Get<ApplicationDbContext>();
            }
            private set
            {
                _dbManager = value;
            }
        }
        private ApplicationUserManager _userManager;
        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.Current.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }


        [Route("ws/api/payment")]
        [HttpPost]
        public async Task<HttpResponseMessage> Payment(PaymentData data)
        {
            try
            {              
                var bank = await DbManager.Banks.SingleOrDefaultAsync(b => b.Id.ToString() == data.BankId);
                if (bank == null)
                {
                    return Request.CreateErrorResponse(HttpStatusCode.Forbidden, "Bank not found");
                }
                var company = await DbManager.Companies.SingleOrDefaultAsync(c => c.IFU == data.IFU);
                var declaration = GetDeclarationFrom(data);

                if (company==null)
                {
                    company = new Company()
                    {
                        IFU = data.IFU,
                        Email=data.Email,
                        Nom=data.CompanyName,
                        PhoneNumber=data.PhoneNumber,
                        CreatedDate=DateTime.Now
                    };
                }
                
                declaration.Company = company;
                declaration.Bank = bank;
                declaration.Status = DeclarationStatus.PENDING;
                DbManager.Declarations.Add(declaration);
                await DbManager.SaveChangesAsync();
                //await SendEmailNotificationToBankAsync(declaration);
                return Request.CreateResponse<PaymentResponse>(HttpStatusCode.OK, new PaymentResponse { Status = 200, Message = DeclarationStatus.PENDING});
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex);
            }
        }

        private async Task SendEmailNotificationToBankAsync(Declaration dec)
        {
            var bankOfficer = await DbManager.Users.FirstOrDefaultAsync(u => u.Bank.Id == dec.Bank.Id && u.Profil==Role.BANK_OFFICER);
            var subject = "Autorisation de paiement de taxes pour la DGI";
            var body = $"Le contribuable {dec.Company.Nom} IFU {dec.Company.IFU} souhaiterait débiter son compte d'un montant de {dec.Amount} XOF pour celui de la DGI";
            EmailSender.Instance.Send(bankOfficer.Email, subject, body, cc:dec.Bank.Email);
        }

        [Route("ws/api/banks")]
        [HttpGet]
        public async Task<HttpResponseMessage> GetBankList(PaymentData data)
        {
            string baseUrl = Request.RequestUri.GetLeftPart(UriPartial.Authority);
            try
            {
                var banks = await DbManager.Banks.ToListAsync();
                var bankResponses = new List<BankResponse>();
                if (banks != null)
                {
                    banks.ForEach(b =>
                        {
                            bankResponses.Add(new BankResponse { Id = b.Id.ToString(), Logo =baseUrl+ b.Logo, Name = b.Name });
                        }
                    );
                }
                return Request.CreateResponse<List<BankResponse>>(HttpStatusCode.OK, bankResponses);
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex);
            }
        }

        [Route("ws/api/payment/status")]
        [HttpPost]
        public async Task<HttpResponseMessage> GetPaymentStatus(PaymentStatusData data)
        {
            try
            {
                var dc = await DbManager.Declarations.FirstOrDefaultAsync(d => d.CartId == data.CartId);
                if (dc == null)
                    return Request.CreateErrorResponse(HttpStatusCode.NotFound, "Payment not found");
                return Request.CreateResponse<PaymentResponse>(HttpStatusCode.OK, new PaymentResponse { Status = 200, Message = dc.Status });
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
        }

        private int GetStatusCodeFrom(string status)
        {
            if (status == DeclarationStatus.PENDING)
                return DeclarationStatus.PENDING_CODE;
            if (status == DeclarationStatus.CONFIRMED)
                return DeclarationStatus.CONFIRMED_CODE;
            if (status == DeclarationStatus.APPROVED)
                return DeclarationStatus.APPROVED_CODE;
            if (status == DeclarationStatus.REJECTED)
                return DeclarationStatus.REJECTED_CODE;
            return 0;
        }

        private Declaration GetDeclarationFrom(PaymentData data)
        {

            var d = new Declaration()
            {
                CartId=data.CartId,
                Amount = long.Parse(data.Amount),
                CreatedDate = DateTime.Now
            };
            return d;
        }
    }
}
