﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Banker.Models
{
    public class Bank
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Logo { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
        public string Adresse { get; set; }
        public DateTime? CreatedDate { get; set; }
    }
}