﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Banker.Models
{
    public class Role
    {
        public static string BANK_OFFICER { get { return "BANK_OFFICER"; } }
        public static string BANK_AUDITOR { get { return "BANK_AUDITOR"; } }
        public static string SUPER_ADMIN { get { return "SUPER_ADMIN"; } }
        public static string DGI_ADMIN { get { return "DGI_ADMIN"; } }
        public static string MAIN_BANK_VALIDATOR { get { return "MAIN_BANK_VALIDATOR"; } }
    }
}