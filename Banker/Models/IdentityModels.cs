﻿using System.Data.Entity;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using Banker.Migrations;
using Z.EntityFramework.Plus;
using System.Threading;

namespace Banker.Models
{
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit http://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class ApplicationUser : IdentityUser
    {
        public string Nom { get; set; }
        public string Prenoms { get; set; }
        public string TempPassword { get; set; }
        public string IP { get; set; }
        public DateTime? CreationDate { get; set; }
        public bool IsValidated { get; set; }
        public DateTime? LastConnectionDate { get; set; }
        public virtual Bank Bank { get; set; }
        public string Profil { get; set; }

        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            return userIdentity;
        } 
        
        public bool IsFirstTimeLogin()
        {
            return !string.IsNullOrEmpty(this.TempPassword);
        }         
    }

    
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext()
            : base("PreProdConnection", throwIfV1Schema: false)
        {
            Database.SetInitializer<ApplicationDbContext>(
                new MigrateDatabaseToLatestVersion<ApplicationDbContext, Configuration>()
                );
        }

        static ApplicationDbContext()
        {
            
        }
        public static ApplicationDbContext Create()
        {
            
            return new ApplicationDbContext();
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
        }

       
        public DbSet<Company> Companies { get; set; }
        public DbSet<Bank> Banks { get; set; }
        public DbSet<Declaration> Declarations { get; set; }

    }
}