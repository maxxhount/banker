﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Banker.Models
{
    public class Company
    {
        public int Id { get; set; }
        public string Nom { get; set; }
        public string IFU { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
        public string TransferNumber { get; set; }
        public DateTime? CreatedDate { get; set; }
    }
}