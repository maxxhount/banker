﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Banker.Models
{
    public class Declaration
    {
        public Declaration()
        {
            Id = Guid.NewGuid().ToString("N");
        }
        public string Id { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? ValidationDate { get; set; }
        public long Amount { get; set; }
        public string CartId { get; set; }
        public string Status { get; set; }
        public string RejectionReason { get; set; }
        public virtual ApplicationUser Validator { get; set; }
        public virtual Bank Bank { get; set; }
        public virtual Company Company { get; set; }
    }

    public class DeclarationStatus
    {
        public static string PENDING { get { return "PENDING"; } }
        public static int PENDING_CODE { get { return 001; } }
        public static string CONFIRMED { get { return "CONFIRMED_BY_OWNER_BANK"; } }
        public static int CONFIRMED_CODE { get { return 002; } }
        public static string APPROVED { get { return "APPROVED"; } }
        public static int APPROVED_CODE { get { return 003; } }
        public static string REJECTED { get { return "REJECTED"; } }
        public static int REJECTED_CODE { get { return 004; } }
    }
}